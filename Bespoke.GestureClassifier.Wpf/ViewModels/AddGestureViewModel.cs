﻿using System;
using System.Windows.Input;
using Prism.Interactivity.InteractionRequest;
using Prism.Mvvm;
using Prism.Commands;

namespace Bespoke.GestureClassifier.Wpf.ViewModels
{
    public class AddGestureViewModel : BindableBase, IConfirmation, IInteractionRequestAware
    {
        public string Title { get; set; }

        public object Content { get; set; }

        public bool Confirmed { get; set; }

        public string GestureName
        {
            get
            {
                return mGestureName;
            }
            set
            {
                SetProperty(ref mGestureName, value);
            }
        }

        public ICommand OkCommand { get; }

        public ICommand CancelCommand { get; }

        public Action FinishInteraction { get; set; }

        public INotification Notification { get; set; }

        public AddGestureViewModel()
        {
            OkCommand = new DelegateCommand(() =>
            {
                Confirmed = true;
                OnFinishInteraction();
            });

            CancelCommand = new DelegateCommand(() =>
            {
                Confirmed = false;
                OnFinishInteraction();
            });
        }

        private void OnFinishInteraction()
        {
            Action handler = FinishInteraction;
            if (handler != null)
            {
                handler();
            }
        }

        private string mGestureName;
    }
}
