﻿using System;
using System.Windows.Input;
using Prism.Mvvm;
using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using Bespoke.GestureClassifier.Framework;

namespace Bespoke.GestureClassifier.Wpf.ViewModels
{
    public class AddCombinationGestureViewModel : BindableBase, IConfirmation, IInteractionRequestAware
    {
        public string Title { get; set; }

        public object Content { get; set; }

        public bool Confirmed { get; set; }

        public string GestureName
        {
            get
            {
                return mGestureName;
            }
            set
            {
                SetProperty(ref mGestureName, value);
                mOkCommand.RaiseCanExecuteChanged();
            }
        }

        public TrainedGesture SelectedLeftHandTrainedGesture
        {
            get
            {
                return mSelectedLeftHandGesture;
            }
            set
            {
                SetProperty(ref mSelectedLeftHandGesture, value);
                mOkCommand.RaiseCanExecuteChanged();
            }
        }

        public TrainedGesture SelectedRightHandTrainedGesture
        {
            get
            {
                return mSelectedRightHandGesture;
            }
            set
            {
                SetProperty(ref mSelectedRightHandGesture, value);
                mOkCommand.RaiseCanExecuteChanged();
            }
        }

        public TrainedGestureCollection LeftHandTrainedGestures { get; }

        public TrainedGestureCollection RightHandTrainedGestures { get; }

        public ICommand OkCommand => mOkCommand;

        public ICommand CancelCommand { get; }

        public Action FinishInteraction { get; set; }

        public INotification Notification { get; set; }

        public AddCombinationGestureViewModel(TrainedGestureCollection leftHandTrainedGestures, TrainedGestureCollection rightHandTrainedGestures)
        {
            LeftHandTrainedGestures = leftHandTrainedGestures;
            RightHandTrainedGestures = rightHandTrainedGestures;

            mOkCommand = new DelegateCommand(() =>
            {
                Confirmed = true;
                OnFinishInteraction();
            }, () => !string.IsNullOrWhiteSpace(GestureName) && SelectedLeftHandTrainedGesture != null && SelectedRightHandTrainedGesture != null);

            CancelCommand = new DelegateCommand(() =>
            {
                Confirmed = false;
                OnFinishInteraction();
            });
        }

        private void OnFinishInteraction()
        {
            Action handler = FinishInteraction;
            if (handler != null)
            {
                handler();
            }
        }

        private string mGestureName;
        private TrainedGesture mSelectedLeftHandGesture;
        private TrainedGesture mSelectedRightHandGesture;
        private DelegateCommand mOkCommand;
    }
}
