﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Prism.Mvvm;
using Bespoke.Common;
using Bespoke.GestureClassifier.Framework;
using WiimoteLib;

namespace Bespoke.GestureClassifier.Wpf.ViewModels
{
    public class ClassificationViewModel : BindableBase, IHandleWiimoteEventsViewModel
    {
        public enum Modes
        {
            LeftHand,
            RightHand,
            BothHands
        }

        private class GestureData
        {
            public WiimoteState CurrentWiimoteState { get; set; }

            public ButtonState CurrentWiimoteButtonState { get; set; }

            public ButtonState LastWiimoteButtonState { get; set; }

            public WiimotePointCollection CurrentWiimoteSamplePoints { get; set; }

            public Gesture Gesture { get; set; }
        }

        public Modes CurrentMode
        {
            get
            {
                return mCurrentMode;
            }
            set
            {
                if (SetProperty(ref mCurrentMode, value))
                {
                    switch (mCurrentMode)
                    {
                        case Modes.LeftHand:
                            mRightHandWiimote.WiimoteChanged -= Wiimote_WiimoteChanged;
                            mLeftHandWiimote.WiimoteChanged += Wiimote_WiimoteChanged;
                            break;

                        case Modes.RightHand:
                        case Modes.BothHands:
                            mLeftHandWiimote.WiimoteChanged -= Wiimote_WiimoteChanged;
                            mRightHandWiimote.WiimoteChanged += Wiimote_WiimoteChanged;                            
                            break;
                    }
                }
            }
        }

        public string ClassifiedGestureName
        {
            get
            {
                return mClassifiedGestureName;
            }
            set
            {
                SetProperty(ref mClassifiedGestureName, value);
            }
        }

        public bool CollectingGesture
        {
            get
            {
                return mCollectingGesture;
            }
            set
            {
                if (SetProperty(ref mCollectingGesture, value))
                {
                    OnPropertyChanged(nameof(HelpText));
                }
            }
        }

        public string HelpText => HelpStrings[CollectingGesture];

        public bool IsClassificationEnabled
        {
            get
            {
                return mIsClassificationEnabled;
            }
            set
            {
                SetProperty(ref mIsClassificationEnabled, value);
            }
        }

        public bool HandleWiimoteEvents
        {
            get
            {
                return mHandleWiimoteEvents;
            }
            set
            {
                if (SetProperty(ref mHandleWiimoteEvents, value))
                {
                    if (mLeftHandWiimote != null)
                    {
                        if (mHandleWiimoteEvents)
                        {
                            mLeftHandWiimote.WiimoteChanged += Wiimote_WiimoteChanged;
                        }
                        else
                        {
                            mLeftHandWiimote.WiimoteChanged -= Wiimote_WiimoteChanged;
                        }
                    }

                    if (mRightHandWiimote != null)
                    {
                        if (mHandleWiimoteEvents)
                        {
                            mRightHandWiimote.WiimoteChanged += Wiimote_WiimoteChanged;
                        }
                        else
                        {
                            mRightHandWiimote.WiimoteChanged -= Wiimote_WiimoteChanged;
                        }
                    }
                }
            }
        }

        public ClassificationViewModel(TrainedGestureSet trainedGestureSet, Wiimote leftHandWiimote, Wiimote rightHandWiimote)
        {
            mTrainedGestureSet = trainedGestureSet;
            mLeftHandWiimote = leftHandWiimote;
            mRightHandWiimote = rightHandWiimote;
            HandleWiimoteEvents = false;
            CurrentMode = Modes.RightHand;

            mLeftHandClassifier = new StatisticalClassifier(mTrainedGestureSet.TrainedGestureCollections[PlayerIndex.One]);
            mRightHandClassifier = new StatisticalClassifier(mTrainedGestureSet.TrainedGestureCollections[PlayerIndex.Two]);

            mLeftHandGestureData = (leftHandWiimote != null ? new GestureData() : null);
            mRightHandGestureData = (rightHandWiimote != null ? new GestureData() : null);
        }

        public async Task UpdateClassifiersAsync()
        {
            try
            {
                if (mLeftHandClassifier.TrainedGestures.Count > 0)
                {
                    await mLeftHandClassifier.TrainedGestures.SetWeightsAsync();
                }

                if (mRightHandClassifier.TrainedGestures.Count > 0)
                {
                    await mRightHandClassifier.TrainedGestures.SetWeightsAsync();
                }

                IsClassificationEnabled = true;
            }
            catch (Exception ex)
            {
                IsClassificationEnabled = false;
                throw new Exception("Error Training Classifier", ex);
            }
        }

        private void Wiimote_WiimoteChanged(object sender, WiimoteChangedEventArgs e)
        {
            switch (CurrentMode)
            {
                case Modes.LeftHand:
                    {
                        UpdateWiimoteStates(mLeftHandGestureData, mLeftHandWiimote.WiimoteState);
                        CollectGesture(mLeftHandGestureData);
                        ClassifiedGesture classifiedGesture = ClassifyGesture(mLeftHandGestureData.Gesture, mLeftHandClassifier);
                        ClassifiedGestureName = (classifiedGesture != null ? classifiedGesture.GestureName : UnknownGestureString);
                        break;
                    }

                case Modes.RightHand:
                    {
                        UpdateWiimoteStates(mRightHandGestureData, mRightHandWiimote.WiimoteState);
                        CollectGesture(mRightHandGestureData);
                        ClassifiedGesture classifiedGesture = ClassifyGesture(mRightHandGestureData.Gesture, mRightHandClassifier);
                        ClassifiedGestureName = (classifiedGesture != null ? classifiedGesture.GestureName : UnknownGestureString);
                        break;
                    }

                case Modes.BothHands:
                    {
                        UpdateWiimoteStates(mLeftHandGestureData, mLeftHandWiimote.WiimoteState);
                        UpdateWiimoteStates(mRightHandGestureData, mRightHandWiimote.WiimoteState);
                        CollectMultiHandGesture(mLeftHandGestureData, mRightHandGestureData);
                        
                        ClassifiedGesture leftHandClassifiedGesture = ClassifyGesture(mLeftHandGestureData.Gesture, mLeftHandClassifier);
                        ClassifiedGesture rightHandClassifiedGesture = ClassifyGesture(mRightHandGestureData.Gesture, mRightHandClassifier);

                        if (leftHandClassifiedGesture != null && rightHandClassifiedGesture != null)
                        {
                            var classifiedMultiGestureQuery = from multiGesture in mTrainedGestureSet.MultiGestures
                                                              where multiGesture.TrainedGestures.ContainsKey(PlayerIndex.One) &&
                                                              multiGesture.TrainedGestures.ContainsKey(PlayerIndex.Two) &&
                                                              multiGesture.TrainedGestures[PlayerIndex.One].GestureName == leftHandClassifiedGesture.GestureName &&
                                                              multiGesture.TrainedGestures[PlayerIndex.Two].GestureName == rightHandClassifiedGesture.GestureName
                                                              select multiGesture;

                            MultiGesture classifiedMultiGesture = classifiedMultiGestureQuery.FirstOrDefault();
                            ClassifiedGestureName = (classifiedMultiGesture != null ? classifiedMultiGesture.GestureName : UnknownGestureString);
                        }
                        else
                        {
                            ClassifiedGestureName = UnknownGestureString;
                        }
                        break;
                    }
            }
        }

        private void UpdateWiimoteStates(GestureData gestureData, WiimoteState wiimoteState)
        {
            gestureData.CurrentWiimoteState = wiimoteState;
            gestureData.LastWiimoteButtonState = gestureData.CurrentWiimoteButtonState;
            gestureData.CurrentWiimoteButtonState = wiimoteState.ButtonState;
        }

        private void CollectGesture(GestureData gestureData)
        {
            bool bButtonPressedThisFrame = ((gestureData.CurrentWiimoteButtonState.B) && (gestureData.LastWiimoteButtonState.B == false));
            bool bButtonReleaseThisFrame = ((gestureData.CurrentWiimoteButtonState.B == false) && (gestureData.LastWiimoteButtonState.B));
            bool bButtonHeldDown = ((gestureData.CurrentWiimoteButtonState.B) && (gestureData.LastWiimoteButtonState.B));

            if (bButtonPressedThisFrame)
            {
                gestureData.CurrentWiimoteSamplePoints = new WiimotePointCollection();
                gestureData.Gesture = null;
                CollectingGesture = true;
            }
            else if ((bButtonReleaseThisFrame))
            {
                gestureData.Gesture = new Gesture(gestureData.CurrentWiimoteSamplePoints);
                CollectingGesture = false;
                ClassifiedGestureName = string.Empty;
            }
            else if (bButtonHeldDown)
            {
                DateTime timestamp = DateTime.Now;
                if (gestureData.CurrentWiimoteSamplePoints.ContainsKey(timestamp) == false)
                {
                    gestureData.CurrentWiimoteSamplePoints.Add(new WiimotePoint(gestureData.CurrentWiimoteState, timestamp));
                }
            }
        }

        private void CollectMultiHandGesture(GestureData leftHandGestureData, GestureData rightHandGestureData)
        {
            bool bButtonPressedThisFrame = ((rightHandGestureData.CurrentWiimoteButtonState.B) && (rightHandGestureData.LastWiimoteButtonState.B == false));
            bool bButtonReleaseThisFrame = ((rightHandGestureData.CurrentWiimoteButtonState.B == false) && (rightHandGestureData.LastWiimoteButtonState.B));
            bool bButtonHeldDown = ((rightHandGestureData.CurrentWiimoteButtonState.B) && (rightHandGestureData.LastWiimoteButtonState.B));

            if (bButtonPressedThisFrame)
            {
                leftHandGestureData.CurrentWiimoteSamplePoints = new WiimotePointCollection();
                leftHandGestureData.Gesture = null;

                rightHandGestureData.CurrentWiimoteSamplePoints = new WiimotePointCollection();
                rightHandGestureData.Gesture = null;

                CollectingGesture = true;
            }
            else if ((bButtonReleaseThisFrame))
            {
                leftHandGestureData.Gesture = new Gesture(leftHandGestureData.CurrentWiimoteSamplePoints);
                rightHandGestureData.Gesture = new Gesture(rightHandGestureData.CurrentWiimoteSamplePoints);
                CollectingGesture = false;
                ClassifiedGestureName = string.Empty;
            }
            else if (bButtonHeldDown)
            {
                DateTime timestamp = DateTime.Now;
                if (leftHandGestureData.CurrentWiimoteSamplePoints.ContainsKey(timestamp) == false)
                {
                    leftHandGestureData.CurrentWiimoteSamplePoints.Add(new WiimotePoint(leftHandGestureData.CurrentWiimoteState, timestamp));
                }
                if (rightHandGestureData.CurrentWiimoteSamplePoints.ContainsKey(timestamp) == false)
                {
                    rightHandGestureData.CurrentWiimoteSamplePoints.Add(new WiimotePoint(rightHandGestureData.CurrentWiimoteState, timestamp));
                }
            }
        }

        private ClassifiedGesture ClassifyGesture(Gesture gesture, IClassifier classifier) => (gesture != null ? classifier.ClassifyGesture(gesture).FirstOrDefault() : null);

        private static readonly Dictionary<bool, string> HelpStrings = new Dictionary<bool, string>() { { false, "Press B to Begin Gesture" }, { true, "Release B to End Gesture" } };
        private static readonly string UnknownGestureString = "-- Unknown Gesture --";

        private Wiimote mLeftHandWiimote;
        private Wiimote mRightHandWiimote;
        private bool mHandleWiimoteEvents;

        private TrainedGestureSet mTrainedGestureSet;
        private StatisticalClassifier mLeftHandClassifier;
        private StatisticalClassifier mRightHandClassifier;
        private bool mCollectingGesture;
        private bool mIsClassificationEnabled;
        private string mClassifiedGestureName;
        private Modes mCurrentMode;

        private GestureData mLeftHandGestureData;
        private GestureData mRightHandGestureData;
    }
}
