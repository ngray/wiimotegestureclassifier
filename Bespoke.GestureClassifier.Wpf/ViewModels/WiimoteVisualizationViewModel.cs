﻿using System;
using Bespoke.Common;
using Prism.Mvvm;
using WiimoteLib;

namespace Bespoke.GestureClassifier.Wpf.ViewModels
{
    public class WiimoteVisualizationViewModel : BindableBase
    {
        public PlayerIndex PlayerIndex
        {
            get
            {
                return mPlayerIndex;
            }
            private set
            {
                SetProperty(ref mPlayerIndex, value);
            }
        }

        public Wiimote Wiimote { get; }

        public int NegativeX
        {
            get
            {
                return mNegativeX;
            }
            private set
            {
                SetProperty(ref mNegativeX, value);
            }
        }

        public int NegativeY
        {
            get
            {
                return mNegativeY;
            }
            private set
            {
                SetProperty(ref mNegativeY, value);
            }
        }

        public int NegativeZ
        {
            get
            {
                return mNegativeZ;
            }
            private set
            {
                SetProperty(ref mNegativeZ, value);
            }
        }

        public int PositiveX
        {
            get
            {
                return mPositiveX;
            }
            private set
            {
                SetProperty(ref mPositiveX, value);
            }
        }

        public int PositiveY
        {
            get
            {
                return mPositiveY;
            }
            private set
            {
                SetProperty(ref mPositiveY, value);
            }
        }

        public int PositiveZ
        {
            get
            {
                return mPositiveZ;
            }
            private set
            {
                SetProperty(ref mPositiveZ, value);
            }
        }

        public WiimoteVisualizationViewModel(PlayerIndex playerIndex, Wiimote wiimote)
        {
            PlayerIndex = playerIndex;
            Wiimote = wiimote;

            if (Wiimote != null)
            {
                Wiimote.WiimoteChanged += Wiimote_WiimoteChanged;
            }
        }

        private void Wiimote_WiimoteChanged(object sender, WiimoteChangedEventArgs e)
        {
            var values = e.WiimoteState.AccelState.Values;

            int x = (int)Math.Min(Math.Abs(values.X) * MaxProgressBarValue, MaxProgressBarValue);
            int y = (int)Math.Min(Math.Abs(values.Y) * MaxProgressBarValue, MaxProgressBarValue);
            int z = (int)Math.Min(Math.Abs(values.Z) * MaxProgressBarValue, MaxProgressBarValue);

            NegativeX = (values.X < 0 ? x : 0);
            PositiveX = (values.X > 0 ? x : 0);
            NegativeY = (values.Y < 0 ? y : 0);
            PositiveY = (values.Y > 0 ? y : 0);
            NegativeZ = (values.Z < 0 ? z : 0);
            PositiveZ = (values.Z > 0 ? z : 0);
        }

        private static readonly int MaxProgressBarValue = 100;

        private PlayerIndex mPlayerIndex;
        private int mNegativeX;
        private int mNegativeY;
        private int mNegativeZ;
        private int mPositiveX;
        private int mPositiveY;
        private int mPositiveZ;
    }
}