﻿using System;
using System.Windows.Input;
using Prism.Commands;
using Prism.Interactivity.InteractionRequest;

namespace Bespoke.GestureClassifier.Wpf.ViewModels
{
    public class AboutViewModel : Notification, IInteractionRequestAware
    {
        public ICommand CloseCommand { get; }

        public Action FinishInteraction { get; set; }

        public INotification Notification { get; set; }

        public string ApplicationTitle { get; set; }

        public string Version { get; set; }

        public string Copyright { get; set; }

        public AboutViewModel()
        {
            CloseCommand = new DelegateCommand(() =>
            {
                Action handler = FinishInteraction;
                if (handler != null)
                {
                    handler();
                }
            });
        }
    }
}
