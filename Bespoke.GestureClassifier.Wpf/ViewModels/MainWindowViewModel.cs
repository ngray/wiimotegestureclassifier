﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Interactivity.InteractionRequest;
using Bespoke.GestureClassifier.Framework;
using WiimoteLib;
using Bespoke.Common;

namespace Bespoke.GestureClassifier.Wpf.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        public string WindowTitle
        {
            get
            {
                return mWindowTitle;
            }
            private set
            {
                SetProperty(ref mWindowTitle, value);
            }
        }

        public bool TrainedGesturesUpdated
        {
            get
            {
                return mTrainedGesturesUpdated;
            }
            private set
            {
                SetProperty(ref mTrainedGesturesUpdated, value);
            }
        }

        public bool TrainedGesturesNotSaved
        {
            get
            {
                return mTrainedGesturesNotSaved;
            }
            private set
            {
                if (SetProperty(ref mTrainedGesturesNotSaved, value))
                {
                    mSaveCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public ICommand NewCommand => mNewCommand;

        public ICommand LoadCommand => mLoadCommand;

        public ICommand SaveCommand => mSaveCommand;

        public ICommand SaveAsCommand => mSaveAsCommand;

        public ICommand AboutCommand => mAboutCommand;

        public ICommand ExitCommand => mExitCommand;

        public IInteractionRequest AboutWindowRequest => mAboutWindowRequest;

        public GestureTrainingViewModel LeftHandGestureTrainingViewModel
        {
            get
            {
                return mLeftHandGestureTrainingViewModel;
            }
            private set
            {
                if (SetProperty(ref mLeftHandGestureTrainingViewModel, value))
                {
                    mSaveCommand.RaiseCanExecuteChanged();
                }                
            }
        }

        public GestureTrainingViewModel RightHandGestureTrainingViewModel
        {
            get
            {
                return mRightHandGestureTrainingViewModel;
            }
            private set
            {
                if (SetProperty(ref mRightHandGestureTrainingViewModel, value))
                {
                    mSaveCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public ClassificationViewModel ClassificationViewModel
        {
            get
            {
                return mClassificationViewModel;
            }
            private set
            {
                SetProperty(ref mClassificationViewModel, value);
            }
        }

        public CombinationTrainingViewModel CombinationTrainingViewModel
        {
            get
            {
                return mCombinationTrainingViewModel;
            }
            private set
            {
                SetProperty(ref mCombinationTrainingViewModel, value);
            }
        }

        public WiimoteVisualizationViewModel LeftHandWiimoteVisualizationViewModel { get; }

        public WiimoteVisualizationViewModel RightHandWiimoteVisualizationViewModel { get; }

        private IMessageBoxService MessageBoxService { get; }

        private IOpenFileDialogService OpenFileDialogService { get; }

        private ISaveFileDialogService SaveFileDialogService { get; }

        private enum Tabs
        {
            LeftHandGestureTraining,
            RightHandGestureTraining,
            CombinationGestures,
            Classification
        }

        public int SelectedTabIndex
        {
            get
            {
                return mSelectedTabIndex;
            }
            set
            {
                if (SetProperty(ref mSelectedTabIndex, value))
                {
                    Tabs selected = (Tabs)mSelectedTabIndex;
                    if (mTabViewModelMapping.ContainsKey(selected))
                    {
                        mTabViewModelMapping[selected].HandleWiimoteEvents = true;
                    }

                    var unSelectedTabs = from viewModelMapping in mTabViewModelMapping
                                         where viewModelMapping.Key != selected
                                         select viewModelMapping;

                    foreach (var unselectedTab in unSelectedTabs)
                    {
                        unselectedTab.Value.HandleWiimoteEvents = false;
                    }
                }
            }
        }

        public MainWindowViewModel(IMessageBoxService messageBoxService, IOpenFileDialogService openFileDialogService, ISaveFileDialogService saveFileDialogService)
        {
            Assert.ParamIsNotNull(messageBoxService);
            Assert.ParamIsNotNull(openFileDialogService);

            MessageBoxService = messageBoxService;
            OpenFileDialogService = openFileDialogService;
            SaveFileDialogService = saveFileDialogService;

            mWindowTitle = "Wiimote Gesture Classifier";
            mNewCommand = DelegateCommand.FromAsyncHandler(OnNewCommandAsync);
            mLoadCommand = DelegateCommand.FromAsyncHandler(OnLoadAsync);
            mSaveCommand = DelegateCommand.FromAsyncHandler(OnSaveAsync, () => TrainedGesturesNotSaved);
            mSaveAsCommand = DelegateCommand.FromAsyncHandler(OnSaveAsAsync);
            mAboutCommand = new DelegateCommand(OnAboutCommand);
            mExitCommand = new DelegateCommand(() => Application.Current.Shutdown());

            mAboutWindowRequest = new InteractionRequest<AboutViewModel>();

            Wiimote leftHandWiimote = ServiceLocator.Current.GetInstance<Wiimote>(PlayerIndex.One.ToString());
            Wiimote rightHandWiimote = ServiceLocator.Current.GetInstance<Wiimote>(PlayerIndex.Two.ToString());

            LeftHandWiimoteVisualizationViewModel = new WiimoteVisualizationViewModel(PlayerIndex.One, leftHandWiimote);
            RightHandWiimoteVisualizationViewModel = new WiimoteVisualizationViewModel(PlayerIndex.Two, rightHandWiimote);

            mNewCommand.Execute().Wait();

            SelectedTabIndex = (int)Tabs.RightHandGestureTraining;
        }

        #region Commands
        private async Task OnNewCommandAsync()
        {
            mTrainedGestureSet = new TrainedGestureSet();
            await RefreshTrainedGestureSetDependenciesAsync();
        }

        private async Task OnLoadAsync()
        {
            OpenFileDialogService.DefaultExt = ".trn";
            OpenFileDialogService.Filter = "Gesture Training files (*.trn)|*.trn|All files (*.*)|*.*";
            OpenFileDialogService.RestoreDirectory = true;

            if (OpenFileDialogService.ShowDialog() == true)
            {
                mTrainedGestureSetFileInfo = new FileInfo(OpenFileDialogService.FileName);
                try
                {
                    mTrainedGestureSet = await TrainedGestureSet.LoadAsync(mTrainedGestureSetFileInfo.FullName);
                    await RefreshTrainedGestureSetDependenciesAsync();
                }
                catch (Exception ex)
                {
                    MessageBoxService.Show(ex.Message, "Error Loading Training Data");
                }
            }
        }

        private async Task OnSaveAsync()
        {
            if (mTrainedGestureSetFileInfo == null)
            {
                await OnSaveAsAsync();
            }
            else
            {
                try
                {
                    await mTrainedGestureSet.SaveAsync(mTrainedGestureSetFileInfo.FullName);
                    TrainedGesturesNotSaved = false;
                }
                catch (Exception ex)
                {
                    MessageBoxService.Show(ex.Message, "Error Saving Training Data");
                }

                try
                {
                    await ClassificationViewModel.UpdateClassifiersAsync();
                    TrainedGesturesUpdated = false;
                }
                catch (Exception ex)
                {
                    MessageBoxService.Show(ex.Message, "Error Saving Training Data");
                    TrainedGesturesUpdated = true;
                }
            }
        }

        private async Task OnSaveAsAsync()
        {
            SaveFileDialogService.Filter = "Trained gesture data files (*.trn)|*.trn";
            SaveFileDialogService.Title = "Save Training Data As";

            if (SaveFileDialogService.ShowDialog() == true)
            {
                mTrainedGestureSetFileInfo = new FileInfo(SaveFileDialogService.FileName);
                WindowTitle = string.Format("Wiimote Gesture Training - {0}", mTrainedGestureSetFileInfo.Name);

                try
                {
                    await mTrainedGestureSet.SaveAsync(mTrainedGestureSetFileInfo.FullName);
                    TrainedGesturesNotSaved = false;
                }
                catch (Exception ex)
                {
                    MessageBoxService.Show(ex.Message, "Error Saving Training Data");
                    TrainedGesturesUpdated = true;
                }

                try
                {
                    await ClassificationViewModel.UpdateClassifiersAsync();
                    TrainedGesturesUpdated = false;
                }
                catch (Exception ex)
                {
                    MessageBoxService.Show(ex.Message, "Error Saving Training Data");
                    TrainedGesturesUpdated = true;
                }
            }
        }

        private void OnAboutCommand()
        {
            mAboutWindowRequest.Raise(new AboutViewModel()
            {
                Title = "About Gesture Classifier",
                ApplicationTitle = "Gesture Classifier",
                Version = "Version 1.0",
                Copyright = "Copyright \u00a9 - Paul Varcholik\nAll Rights Reserved."
            });
        }
        #endregion

        private async Task RefreshTrainedGestureSetDependenciesAsync()
        {
            WindowTitle = string.Format("Wiimote Gesture Training{0}", (mTrainedGestureSetFileInfo != null ? " - " + mTrainedGestureSetFileInfo.Name : string.Empty));

            LeftHandGestureTrainingViewModel = new GestureTrainingViewModel(mTrainedGestureSet, PlayerIndex.One, LeftHandWiimoteVisualizationViewModel.Wiimote, MessageBoxService);
            LeftHandGestureTrainingViewModel.TrainedGesturesUpdated += GestureTraining_TrainedGesturesUpdated;

            RightHandGestureTrainingViewModel = new GestureTrainingViewModel(mTrainedGestureSet, PlayerIndex.Two, RightHandWiimoteVisualizationViewModel.Wiimote, MessageBoxService);
            RightHandGestureTrainingViewModel.TrainedGesturesUpdated += GestureTraining_TrainedGesturesUpdated;

            ClassificationViewModel = new ClassificationViewModel(mTrainedGestureSet, LeftHandWiimoteVisualizationViewModel.Wiimote, RightHandWiimoteVisualizationViewModel.Wiimote);
            try
            {
                await ClassificationViewModel.UpdateClassifiersAsync();
                TrainedGesturesUpdated = false;
            }
            catch (Exception ex)
            {
                MessageBoxService.Show(ex.Message);
                TrainedGesturesUpdated = true;
            }

            CombinationTrainingViewModel = new CombinationTrainingViewModel(mTrainedGestureSet, LeftHandWiimoteVisualizationViewModel.Wiimote, RightHandWiimoteVisualizationViewModel.Wiimote, MessageBoxService);
            CombinationTrainingViewModel.TrainedGesturesUpdated += GestureTraining_TrainedGesturesUpdated;

            mTabViewModelMapping = new Dictionary<Tabs, IHandleWiimoteEventsViewModel>()
            {
                { Tabs.LeftHandGestureTraining, LeftHandGestureTrainingViewModel },
                { Tabs.RightHandGestureTraining, RightHandGestureTrainingViewModel },
                { Tabs.Classification, ClassificationViewModel }
            };

            TrainedGesturesUpdated = false;
            TrainedGesturesNotSaved = false;
        }

        private void GestureTraining_TrainedGesturesUpdated(object sender, EventArgs e)
        {
            TrainedGesturesUpdated = true;
            TrainedGesturesNotSaved = true;
        }

        private GestureTrainingViewModel mLeftHandGestureTrainingViewModel;
        private GestureTrainingViewModel mRightHandGestureTrainingViewModel;
        private ClassificationViewModel mClassificationViewModel;
        private CombinationTrainingViewModel mCombinationTrainingViewModel;

        private FileInfo mTrainedGestureSetFileInfo;
        private string mWindowTitle;

        private DelegateCommand mNewCommand;
        private DelegateCommand mLoadCommand;
        private DelegateCommand mSaveCommand;
        private DelegateCommand mSaveAsCommand;
        private DelegateCommand mAboutCommand;
        private DelegateCommand mExitCommand;

        private InteractionRequest<AboutViewModel> mAboutWindowRequest;

        private TrainedGestureSet mTrainedGestureSet;

        private bool mTrainedGesturesUpdated;
        private bool mTrainedGesturesNotSaved;

        private Dictionary<Tabs, IHandleWiimoteEventsViewModel> mTabViewModelMapping;
        private int mSelectedTabIndex;
    }
}
