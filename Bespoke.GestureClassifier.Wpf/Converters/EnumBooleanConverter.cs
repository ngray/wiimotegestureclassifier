﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Bespoke.GestureClassifier.Wpf
{
    public class EnumBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || parameter == null)
            {
                return false;
            }

            string checkValue = value.ToString();
            string targetValue = parameter.ToString();
            return checkValue.Equals(targetValue, StringComparison.InvariantCultureIgnoreCase);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || parameter == null)
            {
                return null;
            }

            if (value is bool)
            {
                bool boolValue = (bool)value;
                string targetValue = parameter.ToString();
                if (boolValue)
                {
                    return Enum.Parse(targetType, targetValue);
                }
            }

            return null;
        }
    }
}
