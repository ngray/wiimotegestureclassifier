﻿namespace Bespoke.Common
{
    public interface IOpenFileDialogService
    {
        string DefaultExt { get; set; }

        string Filter { get; set; }

        bool RestoreDirectory { get; set; }

        string FileName { get; }

        bool? ShowDialog();
    }
}
