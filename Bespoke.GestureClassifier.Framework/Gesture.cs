﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Bespoke.Common;

namespace Bespoke.GestureClassifier.Framework
{
    public class Gesture
    {
        public enum Features
        {
            Duration,
            MaxX,
            MaxY,
            MaxZ,
            MinX,
            MinY,
            MinZ,
            MeanX,
            MeanY,
            MeanZ,
            MedianX,
            MedianY,
            MedianZ,
            BoundingVolumeLength,
            XYStartingAngleSine,
            XYStartingAngleCosine,
            XZStartingAngleSine,
            XYFirstLastAngleSine,
            XYFirstLastAngleCosine,
            XZFirstLastAngleSine,
            XYTotalAngleTraversed,
            XZTotalAngleTraversed,
            XYTotalAngleTraversedAbsolute,
            XZTotalAngleTraversedAbsolute,
            XYTotalSquaredAngleTraversed,
            XZTotalSquaredAngleTraversed,
            FirstLastPointDistance,
            TotalGestureDistance,
            MaxAccelerationSquared,
        }

        #region Properties

        [Feature(Features.Duration)]
        public double Duration => (mPoints.Values.First().TimeStamp - mPoints.Values.Last().TimeStamp).TotalSeconds;

        [Feature(Features.MaxX)]
        public double MaxX => mPoints.Values.Max(point => point.AccelState.Values.X);

        [Feature(Features.MaxY)]
        public double MaxY => mPoints.Values.Max(point => point.AccelState.Values.Y);

        [Feature(Features.MaxZ)]
        public double MaxZ => mPoints.Values.Max(point => point.AccelState.Values.Z);

        [Feature(Features.MinX)]
        public double MinX => mPoints.Values.Min(point => point.AccelState.Values.X);

        [Feature(Features.MinY)]
        public double MinY => mPoints.Values.Min(point => point.AccelState.Values.Y);

        [Feature(Features.MinZ)]
        public double MinZ => mPoints.Values.Min(point => point.AccelState.Values.Z);

        [Feature(Features.MeanX)]
        public double MeanX => mPoints.Values.Average(point => point.AccelState.Values.X);

        [Feature(Features.MeanY)]
        public double MeanY => mPoints.Values.Average(point => point.AccelState.Values.Y);

        [Feature(Features.MeanZ)]
        public double MeanZ => mPoints.Values.Average(point => point.AccelState.Values.Z);

        [Feature(Features.MedianX)]
        public double MedianX => mPoints.Values.Median(point => point.AccelState.Values.X);

        [Feature(Features.MedianY)]
        public double MedianY => mPoints.Values.Median(point => point.AccelState.Values.Y);

        [Feature(Features.MedianZ)]
        public double MedianZ => mPoints.Values.Median(point => point.AccelState.Values.Z);

        [Feature(Features.BoundingVolumeLength)]
        public double BoundVolumeLength
        {
            get
            {
                double deltaX = MaxX - MinX;
                double deltaY = MaxY - MinY;
                double deltaZ = MaxZ - MinZ;

                return Math.Sqrt((deltaX * deltaX) + (deltaY * deltaY) + (deltaZ * deltaZ));
            }
        }

        [Feature(Features.XYStartingAngleSine)]
        public double XYStartingAngleSine
        {
            get
            {
                WiimotePoint firstPoint = mPoints.Values[0];
                WiimotePoint thirdPoint = (mPoints.Count > 2 ? mPoints.Values[2] : mPoints.Values[1]);
                double distanceBetweenPoints = GetDistance(thirdPoint, firstPoint);

                float deltaY = thirdPoint.AccelState.Values.Y - firstPoint.AccelState.Values.Y;
                return (deltaY == 0.0f ? 0.0 : deltaY / distanceBetweenPoints);
            }
        }

        [Feature(Features.XYStartingAngleCosine)]
        public double XYStartingAngleCosine
        {
            get
            {
                WiimotePoint firstPoint = mPoints.Values[0];
                WiimotePoint thirdPoint = (mPoints.Count > 2 ? mPoints.Values[2] : mPoints.Values[1]);
                double distanceBetweenPoints = GetDistance(thirdPoint, firstPoint);

                float deltaX = thirdPoint.AccelState.Values.X - firstPoint.AccelState.Values.X;
                return (deltaX == 0.0f ? 0.0 : deltaX / distanceBetweenPoints);
            }
        }

        [Feature(Features.XZStartingAngleSine)]
        public double XZStartingAngleSine
        {
            get
            {
                WiimotePoint firstPoint = mPoints.Values[0];
                WiimotePoint thirdPoint = (mPoints.Count > 2 ? mPoints.Values[2] : mPoints.Values[1]);
                double distanceBetweenPoints = GetDistance(thirdPoint, firstPoint);

                float deltaZ = thirdPoint.AccelState.Values.Z - firstPoint.AccelState.Values.Z;
                return (deltaZ == 0.0f ? 0.0 : deltaZ / distanceBetweenPoints);
            }
        }

        [Feature(Features.XYFirstLastAngleSine)]
        public double XYFirstLastAngleSine
        {
            get
            {
                WiimotePoint firstPoint = mPoints.Values[0];
                WiimotePoint lastPoint = mPoints.Values[mPoints.Count - 1];
                double distanceBetweenPoints = GetDistance(lastPoint, firstPoint);

                float deltaY = lastPoint.AccelState.Values.Y - firstPoint.AccelState.Values.Y;
                return (deltaY == 0.0f ? 0.0 : deltaY / distanceBetweenPoints);
            }
        }

        [Feature(Features.XYFirstLastAngleCosine)]
        public double XYFirstLastAngleCosine
        {
            get
            {
                WiimotePoint firstPoint = mPoints.Values[0];
                WiimotePoint lastPoint = mPoints.Values[mPoints.Count - 1];
                double distanceBetweenPoints = GetDistance(lastPoint, firstPoint);

                float deltaX = lastPoint.AccelState.Values.X - firstPoint.AccelState.Values.X;
                return (deltaX == 0.0f ? 0.0 : deltaX / distanceBetweenPoints);
            }
        }

        [Feature(Features.XZFirstLastAngleSine)]
        public double XZFirstLastAngleSine
        {
            get
            {
                WiimotePoint firstPoint = mPoints.Values[0];
                WiimotePoint lastPoint = mPoints.Values[mPoints.Count - 1];
                double distanceBetweenPoints = GetDistance(lastPoint, firstPoint);

                float deltaZ = lastPoint.AccelState.Values.Z - firstPoint.AccelState.Values.Z;
                return (deltaZ == 0.0f ? 0.0 : deltaZ / distanceBetweenPoints);
            }
        }

        [Feature(Features.XYTotalAngleTraversed)]
        public double XYTotalAngleTraversed
        {
            get
            {
                double totalAngleTraversed = 0.0;

                for (int i = 1; i < mPoints.Count - 1; i++)
                {
                    WiimotePoint p = mPoints.Values[i];
                    WiimotePoint pPlus1 = mPoints.Values[i + 1];
                    WiimotePoint pMinus1 = mPoints.Values[i - 1];

                    double dPx = pPlus1.AccelState.Values.X - p.AccelState.Values.X;
                    double dPy = pPlus1.AccelState.Values.Y - p.AccelState.Values.Y;

                    double dPMinus1x = p.AccelState.Values.X - pMinus1.AccelState.Values.X;
                    double dPMinus1y = p.AccelState.Values.Y - pMinus1.AccelState.Values.Y;

                    double denominator = ((dPx * dPMinus1x) + (dPy * dPMinus1y));
                    if (denominator != 0)
                    {
                        totalAngleTraversed += Math.Atan(((dPx * dPMinus1y) - (dPMinus1x * dPy)) / denominator);
                    }
                }

                return totalAngleTraversed;
            }
        }

        [Feature(Features.XZTotalAngleTraversed)]
        public double XZTotalAngleTraversed
        {
            get
            {
                double totalAngleTraversed = 0.0;

                for (int i = 1; i < mPoints.Count - 1; i++)
                {
                    WiimotePoint p = mPoints.Values[i];
                    WiimotePoint pPlus1 = mPoints.Values[i + 1];
                    WiimotePoint pMinus1 = mPoints.Values[i - 1];

                    double dPx = pPlus1.AccelState.Values.X - p.AccelState.Values.X;
                    double dPz = pPlus1.AccelState.Values.Z - p.AccelState.Values.Z;

                    double dPMinus1x = p.AccelState.Values.X - pMinus1.AccelState.Values.X;
                    double dPMinus1z = p.AccelState.Values.Z - pMinus1.AccelState.Values.Z;

                    double denominator = ((dPx * dPMinus1x) + (dPz * dPMinus1z));
                    if (denominator != 0)
                    {
                        totalAngleTraversed += Math.Atan(((dPx * dPMinus1z) - (dPMinus1x * dPz)) / denominator);
                    }
                }

                return totalAngleTraversed;
            }
        }

        [Feature(Features.XYTotalAngleTraversedAbsolute)]
        public double XYTotalAngleTraversedAbsolute
        {
            get
            {
                double totalAngleTraversed = 0.0;

                for (int i = 1; i < mPoints.Count - 1; i++)
                {
                    WiimotePoint p = mPoints.Values[i];
                    WiimotePoint pPlus1 = mPoints.Values[i + 1];
                    WiimotePoint pMinus1 = mPoints.Values[i - 1];

                    double dPx = pPlus1.AccelState.Values.X - p.AccelState.Values.X;
                    double dPy = pPlus1.AccelState.Values.Y - p.AccelState.Values.Y;

                    double dPMinus1x = p.AccelState.Values.X - pMinus1.AccelState.Values.X;
                    double dPMinus1y = p.AccelState.Values.Y - pMinus1.AccelState.Values.Y;

                    double denominator = ((dPx * dPMinus1x) + (dPy * dPMinus1y));
                    if (denominator != 0)
                    {
                        totalAngleTraversed += Math.Abs(Math.Atan(((dPx * dPMinus1y) - (dPMinus1x * dPy)) / denominator));
                    }
                }

                return totalAngleTraversed;
            }
        }

        [Feature(Features.XZTotalAngleTraversedAbsolute)]
        public double XZTotalAngleTraversedAbsolute
        {
            get
            {
                double totalAngleTraversed = 0.0;

                for (int i = 1; i < mPoints.Count - 1; i++)
                {
                    WiimotePoint p = mPoints.Values[i];
                    WiimotePoint pPlus1 = mPoints.Values[i + 1];
                    WiimotePoint pMinus1 = mPoints.Values[i - 1];

                    double dPx = pPlus1.AccelState.Values.X - p.AccelState.Values.X;
                    double dPz = pPlus1.AccelState.Values.Z - p.AccelState.Values.Z;

                    double dPMinus1x = p.AccelState.Values.X - pMinus1.AccelState.Values.X;
                    double dPMinus1z = p.AccelState.Values.Z - pMinus1.AccelState.Values.Z;

                    double denominator = ((dPx * dPMinus1x) + (dPz * dPMinus1z));
                    if (denominator != 0)
                    {
                        totalAngleTraversed += Math.Abs(Math.Atan(((dPx * dPMinus1z) - (dPMinus1x * dPz)) / denominator));
                    }
                }

                return totalAngleTraversed;
            }
        }

        [Feature(Features.XYTotalSquaredAngleTraversed)]
        public double XYTotalSquaredAngleTraversed
        {
            get
            {
                double totalAngleTraversed = 0.0;

                for (int i = 1; i < mPoints.Count - 1; i++)
                {
                    WiimotePoint p = mPoints.Values[i];
                    WiimotePoint pPlus1 = mPoints.Values[i + 1];
                    WiimotePoint pMinus1 = mPoints.Values[i - 1];

                    double dPx = pPlus1.AccelState.Values.X - p.AccelState.Values.X;
                    double dPy = pPlus1.AccelState.Values.Y - p.AccelState.Values.Y;

                    double dPMinus1x = p.AccelState.Values.X - pMinus1.AccelState.Values.X;
                    double dPMinus1y = p.AccelState.Values.Y - pMinus1.AccelState.Values.Y;

                    double denominator = ((dPx * dPMinus1x) + (dPy * dPMinus1y));
                    if (denominator != 0)
                    {
                        totalAngleTraversed += Math.Pow(Math.Atan(((dPx * dPMinus1y) - (dPMinus1x * dPy)) / denominator), 2.0);
                    }
                }

                return totalAngleTraversed;
            }
        }

        [Feature(Features.XZTotalSquaredAngleTraversed)]
        public double XZTotalSquaredAngleTraversed
        {
            get
            {
                double totalAngleTraversed = 0.0;

                for (int i = 1; i < mPoints.Count - 1; i++)
                {
                    WiimotePoint p = mPoints.Values[i];
                    WiimotePoint pPlus1 = mPoints.Values[i + 1];
                    WiimotePoint pMinus1 = mPoints.Values[i - 1];

                    double dPx = pPlus1.AccelState.Values.X - p.AccelState.Values.X;
                    double dPz = pPlus1.AccelState.Values.Z - p.AccelState.Values.Z;

                    double dPMinus1x = p.AccelState.Values.X - pMinus1.AccelState.Values.X;
                    double dPMinus1z = p.AccelState.Values.Z - pMinus1.AccelState.Values.Z;

                    double denominator = ((dPx * dPMinus1x) + (dPz * dPMinus1z));
                    if (denominator != 0)
                    {
                        totalAngleTraversed += Math.Pow(Math.Atan(((dPx * dPMinus1z) - (dPMinus1x * dPz)) / denominator), 2.0);
                    }
                }

                return totalAngleTraversed;
            }
        }

        [Feature(Features.FirstLastPointDistance)]
        public double FirstLastPointDistance => GetDistance(mPoints.Values.Last(), mPoints.Values.First());

        [Feature(Features.TotalGestureDistance)]
        public double TotalGestureDistance
        {
            get
            {
                double totalGestureDistance = 0.0;

                for (int i = 0; i < mPoints.Count - 1; i++)
                {
                    totalGestureDistance += GetDistance(mPoints.Values[i], mPoints.Values[i + 1]);
                }

                return totalGestureDistance;
            }
        }

        [Feature(Features.MaxAccelerationSquared)]
        public double MaxAccelerationSquared
        {
            get
            {
                double maxAccelerationSquared = double.MinValue;

                for (int i = 0; i < mPoints.Count - 1; i++)
                {
                    WiimotePoint firstPoint = mPoints.Values[i];
                    WiimotePoint secondPoint = mPoints.Values[i + 1];
                    TimeSpan dt = secondPoint.TimeStamp - firstPoint.TimeStamp;
                    double accelerationSquared = GetDistance(firstPoint, secondPoint) / (dt.TotalMilliseconds * dt.TotalMilliseconds);

                    if (accelerationSquared > maxAccelerationSquared)
                    {
                        maxAccelerationSquared = accelerationSquared;
                    }
                }

                return maxAccelerationSquared;
            }
        }

        public WiimotePoint[] Points => mPoints.ToArray();

        #endregion

        static Gesture()
        {
            var featureProperties = from propertyInfo in typeof(Gesture).GetProperties()
                                    let featureAttribute = propertyInfo.GetCustomAttribute(typeof(FeatureAttribute)) as FeatureAttribute
                                    where featureAttribute != null
                                    select new { featureAttribute.Feature, PropertyInfo = propertyInfo };

            sFeatureProperties = featureProperties.ToDictionary(featureProperty => featureProperty.Feature, featureProperty => featureProperty.PropertyInfo);
        }

        public Gesture(WiimotePointCollection points)
        {
            Assert.ParamIsNotNull(nameof(points), points);
            Assert.IsTrue("points.Count", points.Count > 1);

            mPoints = points;
        }

        #region Public Methods

        public static async Task<Gesture> LoadAsync(BinaryReader reader)
        {
            WiimotePointCollection points = new WiimotePointCollection();

            await Task.Run(() =>
            {
                int totalPoints = reader.ReadInt32();
                for (int i = 0; i < totalPoints; i++)
                {
                    points.Add(WiimotePoint.Load(reader));
                }
            });

            return new Gesture(points);
        }

        public static double GetFeatureValueFromSample(Features feature, Gesture sample)
        {
            if (sFeatureProperties.ContainsKey(feature) == false)
            {
                throw new Exception(string.Format("Feature [{0}] not found", feature));
            }

            return (double)sFeatureProperties[feature].GetValue(sample, null);
        }

        public double GetFeatureValue(Features feature) => GetFeatureValueFromSample(feature, this);

        public double[] GetFeatureVector()
        {
            var featureVector = from feature in (Gesture.Features[])Enum.GetValues(typeof(Gesture.Features))
                                let featureValue = GetFeatureValue(feature)
                                select featureValue;            

            return featureVector.ToArray();
        }

        public async Task SaveAsync(BinaryWriter writer)
        {
            await Task.Run(() =>
            {
                writer.Write(mPoints.Count);

                foreach (WiimotePoint point in mPoints.Values)
                {
                    point.Save(writer);
                }
            });
        }

        #endregion

        private double GetDistance(WiimotePoint point1, WiimotePoint point2)
        {
            double dx = point1.AccelState.Values.X - point2.AccelState.Values.X;
            double dy = point1.AccelState.Values.Y - point2.AccelState.Values.Y;
            double dz = point1.AccelState.Values.Z - point2.AccelState.Values.Z;

            return Math.Sqrt((dx * dx) + (dy * dy) + (dz * dz));
        }

        private static Dictionary<Features, PropertyInfo> sFeatureProperties;
        private WiimotePointCollection mPoints;
    }

    public class GestureCollection : List<Gesture>
    {
        public static async Task<GestureCollection> LoadAsync(string fileName)
        {
            if (File.Exists(fileName) == false)
            {
                throw new FileNotFoundException();
            }

            using (FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                return await LoadAsync(new BinaryReader(fileStream));
            }
        }

        public static async Task<GestureCollection> LoadAsync(BinaryReader reader)
        {
            GestureCollection gestures = new GestureCollection();

            int totalGestures = reader.ReadInt32();
            for (int i = 0; i < totalGestures; i++)
            {
                Gesture gesture = await Gesture.LoadAsync(reader);
                gestures.Add(gesture);
            }

            return gestures;
        }

        public async Task SaveAsync(string fileName)
        {
            using (FileStream file = new FileStream(fileName, FileMode.Create, FileAccess.Write))
            {
                BinaryWriter writer = new BinaryWriter(file);
                await SaveAsync(writer);
            }
        }

        public async Task SaveAsync(BinaryWriter writer)
        {
            writer.Write(Count);
            foreach (Gesture gesture in this)
            {
                await gesture.SaveAsync(writer);
            }
        }
    }
}
