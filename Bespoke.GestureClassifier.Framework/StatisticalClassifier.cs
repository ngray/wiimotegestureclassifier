﻿using System;
using System.Linq;
using System.Collections.Generic;
using Bespoke.Common;

namespace Bespoke.GestureClassifier.Framework
{
    public class StatisticalClassifier : IClassifier
    {
        public TrainedGestureCollection TrainedGestures
        {
            get
            {
                return mTrainedGestures;
            }
            set
            {
                Assert.ParamIsNotNull(nameof(TrainedGestures), value);
                mTrainedGestures = value;
            }
        }

        public StatisticalClassifier(TrainedGestureCollection trainedGestures)
        {
            TrainedGestures = trainedGestures;
        }

        public ClassifiedGestureCollection ClassifyGesture(Gesture gesture)
        {
            Assert.ParamIsNotNull(nameof(gesture), gesture);

            Dictionary<TrainedGesture, double> gestureEvaluations = new Dictionary<TrainedGesture, double>();
            foreach (TrainedGesture trainedGesture in TrainedGestures)
            {
                double evaluation = trainedGesture.InitialWeight +
                                    (from feature in (Gesture.Features[])Enum.GetValues(typeof(Gesture.Features))
                                     select trainedGesture.Weights[feature] * gesture.GetFeatureValue(feature)).Sum();

                gestureEvaluations.Add(trainedGesture, evaluation);
            }

            KeyValuePair<TrainedGesture, double> maxEvaluation = (from gestureEvaluation in gestureEvaluations
                                                                  orderby gestureEvaluation.Value descending
                                                                  select gestureEvaluation).FirstOrDefault();

            ClassifiedGestureCollection classifiedGestureSet = new ClassifiedGestureCollection();
            if (maxEvaluation.Key != null)
            {
                classifiedGestureSet.Add(new ClassifiedGesture(maxEvaluation.Key.GestureName, 100.0));
            }

            return classifiedGestureSet;
        }

        private TrainedGestureCollection mTrainedGestures;
    }
}
